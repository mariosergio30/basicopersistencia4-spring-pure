package curso;



import curso.model.Aluno;
import curso.model.AlunoEntity;
import curso.service.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Properties;


//@ComponentScan("curso")
//@ComponentScan
//@Component
public class Application {

    public static void main(String[] args) {

       /*  BEANS INSTANTIATION APENAS POR ANNOTATIONS
       ApplicationContext  applicationContext =
               new AnnotationConfigApplicationContext(Application.class);
       */

        /*  BEANS INSTANTIATION APENAS POR XML + ANNOTATIONS (<context:annotation-config/>) */
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-beans.xml");

        for (String beanName : applicationContext.getBeanDefinitionNames()) {
            System.out.println(beanName);
        }

        applicationContext.getBean(Application.class).executa();
    }


    public void executa () {

        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< INICIO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println("MAIN RUNNING ......................");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< FIM >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

    }





}
