package curso.configuration;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanCustomConfiguration {


    /*@PersistenceUnit
    private EntityManagerFactory factory;
*/

    @Bean(name = "dbMysql")
    public EntityManager entityManagerMysql () {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("dbEscola");
        return factory.createEntityManager();
    }


/*

    @Bean(name = "dbOracle")
    public EntityManager entityManagerOracle () {

        // PODERIAMOS TER UMA CONEXÃO COM OUTRO DATABASE
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("dbEscola");
        return factory.createEntityManager();
    }
*/


    /*
    @Bean
    public EntityManagerFactory entityManagerFactory () {
        return Persistence.createEntityManagerFactory("dbEscola");
    }
    */


    /*
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory()  {

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();

        factory.setPackagesToScan("curso");

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        factory.setJpaVendorAdapter(vendorAdapter);


        //JpaVendorAdapter vendor = new HibernateJpaVendorAdapter();
        //factory.setJpaVendorAdapter(vendor);


        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setUsername("root");
        ds.setPassword("root");
        ds.setUrl("jdbc:mysql://localhost:3306/casadocodigo");
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        factory.setDataSource(ds);

        Properties prop = new Properties();
        prop.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        prop.setProperty("hibernate.show_sql", "true");
        prop.setProperty("hibernate.hbm2ddl.auto", "update");
        factory.setJpaProperties(prop);

        return factory;
    }
    */



}
