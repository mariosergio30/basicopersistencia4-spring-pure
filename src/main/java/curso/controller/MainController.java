package curso.controller;


import java.time.LocalTime;
import java.util.List;

import curso.dao.AlunoRepository;
import curso.model.AlunoEntity;
import curso.service.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.DependsOn;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;

//@Controller
public class MainController {

    @Autowired
    private AlunoService alunosService;

    /*
    @Autowired // OBS para funcionar teriamos que alterar o bean EntityManager para LocalContainerEntityManagerFactoryBean
    private AlunoRepository alunoRepository;
   */

    @Autowired
    private Environment environment;

    @PostConstruct
    public void init() {


    	LocalTime currentTime = LocalTime.now();
    	System.out.println(currentTime.toString() + " ########## MAINCONTROLER INIT ########## ");
        /*
    	System.out.println(currentTime.toString() + " ########## " + environment.getProperty("spring.application.name") + ":" + environment.getProperty("server.port") + " ##########");    	
		System.out.println(currentTime.toString() + " ########## try: localhost:" + environment.getProperty("server.port") + "/swagger-ui.html ##########");
		*/

        this.run();
    }



    public void run () {

        //List<AlunoEntity> a = alunoRepository.teste();

        System.out.println(" ########## MAINCONTROLER RUNNING ########## ");

        // ConexaoJDBC con = conectaDB(); //  SUBSTITUIDO PELO EntityManager + persistenceOLD_JAVAX.xml do HIBERNATE

        /*
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("dbEscola");
        entityManager = factory.createEntityManager();

        //alunosService = new AlunoService(entityManager);

        */

        // consultaDisciplinas();

        // inserirAlunos();

        consultaTodosAlunos();

        consultaUmAluno("00000005");


        /* DO DO
        updateAluno();
        inserirDisciplina();
		inserirProfessor();

        consultaProfessoresByDisciplina();

        consultaOneAula();   // retorna também nome do professor e da disciplina

        consultaAulas();   // retorna também nome do professor e da disciplina
        */

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< FIM >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");


    }


    /**
     public static ConexaoJDBC conectaDB() {

     //  SUBSTITUIDO PELO persistenceOLD_JAVAX.xml do HIBERNATE

     }
     **/

    public  boolean inserirAlunos() {

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de INSERT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;

        AlunoEntity aluno1 = new AlunoEntity();
        aluno1.setNome("Aluno Exemplo");
        aluno1.setIdade(21);
        aluno1.setCpf("000.001.003.14");
        aluno1.setMatricula("78822666");
        aluno1.setSexo("M");

        if (alunosService.matricular(aluno1)) {
            System.out.println("OK");
            return true;
        }

        return false;

    }


    public  void consultaDisciplinas() {

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL Disciplinas  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println("Consultando primeira e ultima disciplina...");

        System.out.println("TO DO");

    }




    public  void consultaTodosAlunos() {


        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL (com retorno Todos os Registros)  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;


        List<AlunoEntity> listAlunos = alunosService.consultaAlunos();

        for(AlunoEntity a : listAlunos){
            System.out.println("Nome " + a.getNome() + " - matricula " + a.getMatricula());

        }


    }


    public  void consultaUmAluno(String matricula) {

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL (Um Aluno)  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;

        AlunoEntity aluno = alunosService.consultaAlunoPorMatricula(matricula);

        if (aluno != null) {

            System.out.println("Nome: " + aluno.getNome() + " CPF: " + aluno.getCpf());
        }

    }


}

