package curso.dao;

import curso.model.AlunoEntity;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
/*  OBS: para funcionar teriamos que alterar o bean EntityManager para LocalContainerEntityManagerFactoryBean */
public interface AlunoRepository extends JpaRepository<AlunoEntity, String> {

    @Query("SELECT a FROM AlunoEntity a")
    public List<AlunoEntity> teste();


}
