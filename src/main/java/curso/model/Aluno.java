package curso.model;


import org.springframework.stereotype.Component;


public final class Aluno  {
	
    private String matricula = "";
    private boolean pne = false;

    
    public String getMatricula() {
        return matricula;
    }
        
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    
    public boolean isPne() {
        return pne;
    }
        
    public void setPne(boolean pne) {
        this.pne = pne;
    }
    
    
    
}
